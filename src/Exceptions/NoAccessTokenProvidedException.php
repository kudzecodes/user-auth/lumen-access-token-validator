<?php

namespace Kudze\LumenAccessTokenValidator\Exceptions;

use Exception;

class NoAccessTokenProvidedException extends Exception
{

}