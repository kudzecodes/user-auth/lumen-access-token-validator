<?php

namespace Kudze\LumenAccessTokenValidator\Traits;

use DomainException;
use Exception;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\SignatureInvalidException;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Kudze\AccessTokenValidator\Exception\InvalidTokenTypeException;
use Kudze\AccessTokenValidator\Model\User;
use Kudze\AccessTokenValidator\Service\AccessTokenValidator;
use Kudze\LumenAccessTokenValidator\Exceptions\NoAccessTokenProvidedException;
use UnexpectedValueException;

trait DecodesTokens
{
    protected function getAccessTokenFromRequest(Request $request): ?string
    {
        $authorization = $request->header('Authorization');
        if ($authorization === null)
            return null;

        return str_replace("Bearer ", "", $authorization);
    }

    /**
     * @throws NoAccessTokenProvidedException Provided request does not contain Authorization header
     * @throws InvalidTokenTypeException      Provided token is refresh token
     * @throws InvalidArgumentException       Provided key/key-array was empty or malformed
     * @throws DomainException                Provided JWT is malformed
     * @throws UnexpectedValueException       Provided JWT was invalid
     * @throws SignatureInvalidException      Provided JWT was invalid because the signature verification failed
     * @throws BeforeValidException           Provided JWT is trying to be used before it's eligible as defined by 'nbf'
     * @throws BeforeValidException           Provided JWT is trying to be used before it's been created as defined by 'iat'
     * @throws ExpiredException               Provided JWT has since expired, as defined by the 'exp' claim
     */
    protected function decodeUserFromToken(AccessTokenValidator $accessTokenValidator, Request $request): User
    {
        $accessToken = $this->getAccessTokenFromRequest($request);
        if ($accessToken === null)
            throw new NoAccessTokenProvidedException("Access token was not provided!");

        return $accessTokenValidator->decodeUserFromAccessToken($accessToken);
    }
}